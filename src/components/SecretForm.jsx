import React, { useRef, useState } from 'react'

export default function SecretForm({sendSecret, displayError}) {

    const inputRef = useRef(null)

    const handleSubmit = function(e) {
        e.preventDefault()
        const formData = new FormData(e.target)
        sendSecret(formData.get('secret'))
        inputRef.current.value = ''
    }

    return (
        <form className='d-flex flex-column' onSubmit={handleSubmit}>
            <div className="position-relative mb-4">
                <input className="form-control text-center" type="text" placeholder={'000000'} max={6} name={'secret'} ref={inputRef}/>
                <div className={['position-absolute w-100 h-100 top-0 start-0 bg-body text-danger fw-bold justify-content-center align-items-center ' + (displayError ? 'd-flex' : 'd-none')]}>
                    {displayError && 'Wrong code!'}
                </div>
            </div>
            <input className="btn btn-danger" type="submit" value="Crack the code"/>
        </form>
    )
}