import React, {useEffect, useRef, useState} from 'react'

import Countdown, {zeroPad} from 'react-countdown';

const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
        return <div>File transfer completed.</div>;
    } else {
        return <span>{zeroPad(hours)}:{zeroPad(minutes)}:{zeroPad(seconds)}</span>;
    }
};

export default function CountDown({startDate, stop}) {
    const countdownElement = useRef(null)

    useEffect(() => {
        if (stop) {
            countdownElement.current.pause()
        }
    })

    let delay = 2 * 60 * 60 * 1000;

    return <div className="fs-2 text-center fw-bold mb-4"><Countdown date={startDate + delay} renderer={renderer} ref={countdownElement}/></div>
}