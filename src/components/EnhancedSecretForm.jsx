import React, { useState } from 'react'
import EnhancedSecretInput from "./EnhancedSecretInput";

export default function EnhancedSecretForm({sendSecret}) {
    const [secretValues, setValues] = useState({});

    const handleChange = (e) => {
        const { maxLength, value, name } = e.target;
        const [fieldName, fieldIndex] = name.split("-");

        if (value.length >= maxLength) {
            if (parseInt(fieldIndex) < 6) {
                const nextSibling = document.querySelector(
                    `input[name=secret-${parseInt(fieldIndex) + 1}]`
                );

                if (nextSibling !== null) {
                    nextSibling.focus();
                }
            }
        }

        setValues({
            ...secretValues,
            [`secret-${fieldIndex}`]: value
        });
    }

    const handleSubmit = function(e) {
        e.preventDefault()
        const formData = new FormData(e.target)
        let code = ""
        for(let pair of formData.entries()) {
            let value = pair[1];
            code += pair[1];
        }

        sendSecret(code)
    }

    return (
        <form className='d-flex rounded border' onSubmit={handleSubmit}>
            <EnhancedSecretInput name='secret-0' onChange={handleChange}/>
            <EnhancedSecretInput name='secret-1' onChange={handleChange}/>
            <EnhancedSecretInput name='secret-2' onChange={handleChange}/>
            <EnhancedSecretInput name='secret-3' onChange={handleChange}/>
            <EnhancedSecretInput name='secret-4' onChange={handleChange}/>
            <EnhancedSecretInput name='secret-5' onChange={handleChange}/>
            <input type="submit" style={{display: 'none'}}/>
        </form>
    )
}