import React from 'react'

export default function EnhancedSecretInput({name, onChange}) {
    function handleChange(e) {
        e.target.value = e.nativeEvent.data
        onChange(e)
    }

    return <input type="text" className='input-raw' min={0} max={9} pattern="[0-9]" placeholder={0} name={name} id={name} onChange={handleChange}/>
}