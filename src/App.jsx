import React, { useEffect, useRef, useState } from 'react'
import './App.scss'

import CountDown from "./components/Countdown";
import SecretForm from "./components/SecretForm";
import Countdown, { zeroPad } from 'react-countdown'

const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
        return <div>File transfer completed.</div>;
    } else {
        return <span>{zeroPad(hours)}:{zeroPad(minutes)}:{zeroPad(seconds)}</span>;
    }
};

function App() {
    let startDate = parseInt(localStorage.getItem('startDate'));

    if (!startDate) {
        startDate = Date.now();
        localStorage.setItem('startDate', startDate);
    }

    let [founded, setFounded] = useState(localStorage.getItem('savedFounded') === '1')
    let [displayError, setDisplayError] = useState(false)
    let [endOfGame, setEndOfGame] = useState(founded)

    function getSecretInput(e) {
        const valueCheck = e === "190891"
        setFounded(valueCheck)
        setEndOfGame(valueCheck)
        localStorage.setItem('savedFounded', valueCheck ? '1' : '0')
        localStorage.setItem('timing', Date.now() - startDate)

        if (!valueCheck) {
            setDisplayError(true)
            setTimeout(() => setDisplayError(false), 1000)
        }
    }

    function resetGame() {
        localStorage.clear()
        window.location.reload()
    }

    if (founded) {
        startDate = Date.now() - parseInt(localStorage.getItem('timing'))
    }

    const countdownElement = useRef(null)

    useEffect(() => {
        if (founded) {
            countdownElement.current.pause()
        }

        console.log(countdownElement.current.isCompleted())
        if (countdownElement.current.isCompleted()) {
            setEndOfGame(true)
        }
    })

    let delay = 2 * 60 * 60 * 1000;

    const playAgain = <button onClick={resetGame} className="btn btn-primary">Play again</button>

    return (
        <div className="App">
            <div className="fs-2 text-center fw-bold mb-4"><Countdown date={startDate + delay} renderer={renderer} ref={countdownElement} onComplete={() => setEndOfGame(true)}/></div>
            <div className="position-relative">
                {!endOfGame && <SecretForm sendSecret={getSecretInput} displayError={displayError}/>}
            </div>
            <div className={'mb-4'}>
                {founded && 'Well done! File transfer stopped!'}
            </div>
            <div className={'text-center'}>
                {endOfGame && playAgain}
            </div>
        </div>
    )
}

export default App
